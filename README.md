# Andalas #

## Summary ##
Andalas is a simple functional scripting language on top of Python designed for educational purpose. Andalas is made by a student in his free time as a project for understanding Python and it's capability. Andalas is still a work in progress and far from being a proper scripting language.

## Setting up ##
To install Andalas, download the package and run the setup.py python script from a terminal.
```
python3 setup.py install
```

## Example Script ##

### Andalas interpreter ###
"Hello world" in interpreter:
```
Andalas 0.0.1
~~~ print "Hello world"
Hello world
```

"Hello world" with variables:
```
Andalas 0.0.1
~~~ a = "Hello"
Hello
~~~ b = "world"
world
~~~ print a b
Hello world
```

### Andalas script ###
hundred.and:
```
a = 10
print "a is" a
b = 2
print "b is" b
c = a ^ b
print "c is" c
```

Executing hundred.and from terminal:
```
$ andalas hundred.and
a is 10.0
b is 2.0
c is 100.0
```

## To do ##

* Add branching and looping
* Add list literal support for expression
* Add function definition

## About ##

### Author ###
I'm currently pursuing a bachelor's degree at Bandung Institute of Technology. I started programming since I was ten years old. Although it has almost been a decade since, I still have a long way to go to achieve my dream of starting my own video games studio. As an undergraduate student, I'm exploring deeper into the endless possibility of programming.