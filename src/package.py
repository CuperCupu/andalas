name='Andalas'
version='0.0.2'
author='Suhendi'
author_email='suhendi999@gmail.com'
url="https://bitbucket.org/CuperCupu/andalas"
packages=['andalas']
package_dir={
    "andalas":"src/",
}
python_requires="~= 3.6.2"
entry_points={
    "console_scripts": [
        "andalas = andalas.interpreter:main",
    ]
}