#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
This module handles expression
'''
import sys
from enum import Enum
from inspect import signature
from functools import reduce
from functools import partial
import math

class TokenError(SyntaxError):
    '''
    Raised when an error related to tokens occured.
    '''
    pass

class Operator:

    def __init__(self, name, arg_count, func, aliases = None):
        self.name = name
        self.aliases = [self.name]
        if aliases:
            self.aliases += aliases
        self.func = func
        self.arg_count = arg_count

    def __call__(self, *args):
        return self.func(*args)

    def text(self, mode, *args):
        if mode == Expression.INFIX:
            pass
    
    def __repr__(self):
        return self.name
    
    def __len__(self):
        return self.arg_count

class OperatorSet(list):

    def __init__(self, operators: list):
        self.extend(operators)
        self.operators = {}
        for op in operators:
            if isinstance(op, Operator):
                for alias in op.aliases:
                    self.operators[alias] = op
            else:
                raise TypeError(Operator.__name__ + " expected, got " + op.__class__.__name__ + " when appending the set of operators")
    
    def __getitem__(self, key):
        return self.operators[key]

    def __setitem__(self, key, value):
        self.operators[key] = value

    def __contains__(self, key):
        return key in self.operators

class Expression:
    '''
    A class for representating an expression.
    An expression object contains expression mode and operators set.
    Modes:
        POSTFIX
        PREFIX
        INFIX
    '''
    # Constant for expression arrangement mode.
    POSTFIX = 0
    PREFIX = 1
    INFIX = 2

    # Constant for the prefex denotating special value.
    VAR_PREFIX = "@"
    EXP_PREFIX = "#"

    # Constant for sets of operators.
    SET_DEFAULT = OperatorSet([
        Operator("+", 2, lambda x, y: x + y, ["add"]),
        Operator("-", 2, lambda x, y: x - y, ["sub"]),
        Operator("*", 2, lambda x, y: x * y, ["mul"]),
        Operator("/", 2, lambda x, y: x / y, ["div"]),
        Operator("^", 2, lambda x, y: x ** y, ["pow"]),
        Operator("%", 2, lambda x, y: x % y, ["mod"]),
        Operator("sqr", 1, lambda x: x ** 2),
        Operator("sqrt", 1, lambda x: x ** 0.5),
        Operator("abs", 1, abs),
    ])

    # Constant for keyword argument.
    KW_OPERATORS = "operators"
    KW_EXPRESSION = "expression"
    KW_MODE = "mode"

    # List of operators which second arg will be added parentheses when converting to INFIX mode.
    priority = ["*", "/", "^"]

    @staticmethod
    def fixformat(string, operators=None):
        # Fix open parentheses
        i = string.find("(")
        pos = 0
        while i > 0:
            if string[i - 1] != "(" and string[i - 1] != " ":
                string = string[:i] + " " + string[i:]
                pos += 1
            pos = i + 1
            i = string.find("(", pos)
        # Fix close parentheses
        i = string.find(")")
        pos = 0
        while i > 0 and i < len(string) - 1:
            if string[i + 1] != ")" and string[i + 1] != " ":
                string = string[:i+1] + " " + string[i+1:]
                pos += 1
            pos += i + 1
            i = string.find(")", pos)
        if operators:
            operators_alias = []
            for op in operators:
                operators_alias += op.aliases
            single = []
            for k in operators_alias:
                if len(k) == 1:
                    single.append(k)
            for k in single:
                i = string.find(k)
                pos = 0
                while i > 0 and i < len(string):
                    if string[i - 1] != " ":
                        poss = i - 1
                        while poss > 0 and poss != " ":
                            poss -= 1
                        tok = string[poss:i+1]
                        if tok not in operators:
                            string = string[:i] + " " + string[i:]
                            pos += 1
                            i += 1
                    if i < len(string) - 1 and string[i + 1] != " ":
                        poss = i + 1
                        while poss < len(string) and poss != " ":
                            poss += 1
                        tok = string[i:poss+1]
                        if tok not in operators:
                            string = string[:i+1] + " " + string[i+1:]
                    pos = i + 1
                    i = string.find(k, pos)

        return string

    def __get_next_expression_key(self):
        while str(self.__exp_count) in self.expression:
            self.__exp_count += 1
        return str(self.__exp_count)

    def __init__(self, exp, **kwargs):
        if Expression.KW_OPERATORS in kwargs:
            # Set custom operator sets.
            self.operators = kwargs[Expression.KW_OPERATORS]
        else:
            # Set operator to standard.
            self.operators = Expression.SET_DEFAULT
        if not isinstance(self.operators, OperatorSet):
            raise TypeError("incompatible type {}, expected {}".format(self.operators.__class__.__name__, OperatorSet.__name__))
        if Expression.KW_EXPRESSION in kwargs:
            self.expression = kwargs[Expression.KW_EXPRESSION]
        else:
            # Default to no nested expression.
            self.expression = {}

        mode = Expression.INFIX
        if 'mode' in kwargs:
            mode = kwargs['mode']

        self.__exp_count = 0
        # the string of the expression.
        l = ""
        if isinstance(exp, str):
            while exp[0] == "(" and exp[len(exp) - 1] == ")":
                exp = exp[1:][:-1]
            l = exp
        elif isinstance(exp, list):
            l = reduce(lambda x, y: str(x) + " " + str(y), exp)
        self.mode = mode
        tokens = self.parse(l)
        self.root = Expression.BuildTokenRoot(tokens, self.operators, self.mode)

    class Token:

        class TokenType(Enum):
            OPERATOR = 0
            VALUE = 1

        def __init__(self, t_type, val, params=None):
            self.type = t_type
            if self.type == Expression.Token.TokenType.VALUE:
                self.value = val
            elif self.type == Expression.Token.TokenType.OPERATOR:
                if not isinstance(val, Operator):
                    raise TypeError("incompatible type: expected {}, got {}".format(Operator.__name__, val.__class__.__name__))
                self.operator = val
                self.values = []
                for v in params:
                    if not isinstance(v, Expression.Token):
                        raise TypeError("incompatible type: expected {}, got {}".format(Expression.Token.__name__, v.__class__.__name__))
                    self.values.append(v)
            else:
                raise NameError("unknown token type {}".format(t_type))
        
        def __repr__(self):
            return self.text()

        def text(self, mode=2):
            tokens = self.collapse(mode)
            res = ""
            for t in tokens:
                if res:
                    res += " "
                if isinstance(t, Expression.Token):
                    res += t.text(mode)
                else:
                    res += str(t)
            return res

        def collapse(self, mode=2):
            if self.type == Expression.Token.TokenType.VALUE:
                tokens = [self.value]
            elif self.type == Expression.Token.TokenType.OPERATOR:
                tokens = []
                vtokens = []
                otoken = []
                sep = -1
                for v in self.values:
                    if isinstance(v, Expression.Token):
                        vtokens += v.collapse(mode)
                    else:
                        vtokens += [v]
                    if sep == -1:
                        sep = len(vtokens)
                if self.operator != None:
                    otoken = [self.operator.name]
                if mode == Expression.INFIX:
                    tokens += vtokens[:sep] + otoken
                    rest = vtokens[sep:]
                    if len(rest) == 1:
                        tokens += rest
                    else:
                        tokens += [rest]
                elif mode == Expression.POSTFIX:
                    tokens += vtokens + otoken
                elif mode == Expression.PREFIX:
                    tokens += otoken + vtokens
            return tokens

    @staticmethod
    def BuildTokenRoot(tokens, operators, mode=0):
        tokens = list(tokens)
        stack = []
        if mode == Expression.POSTFIX:
            while tokens:
                if tokens[0] in operators:
                    op = operators[tokens[0]]
                    t = Expression.Token(Expression.Token.TokenType.OPERATOR, op, stack[-len(op):])
                    stack = stack[:-len(op)] + [t]
                else:
                    stack.append(Expression.Token(Expression.Token.TokenType.VALUE, tokens[0]))
                tokens = tokens[1:]
        elif mode == Expression.PREFIX:
            def read_token(tokens):
                retval = None
                if tokens[0] in operators:
                    op = operators[tokens[0]]
                    tokens = tokens[1:]
                    vals = []
                    i = len(op)
                    while i > 0:
                        val, tokens = read_token(tokens)
                        vals.append(val)
                        i -= 1
                    retval = Expression.Token(Expression.Token.TokenType.OPERATOR, op, vals)
                else:
                    retval = Expression.Token(Expression.Token.TokenType.VALUE, tokens[0])
                    tokens = tokens[1:]
                return retval, tokens
            stack.append(read_token(tokens)[0])
        elif mode == Expression.INFIX:
            op_stack = []
            while tokens:
                if tokens[0] in operators:
                    op_stack.append(operators[tokens[0]])
                else:
                    nval = Expression.Token(Expression.Token.TokenType.VALUE, tokens[0])
                    stack.append(nval)
                    if op_stack:
                        top = op_stack.pop()
                        if len(stack) >= len(top):
                            t = Expression.Token(Expression.Token.TokenType.OPERATOR, top, stack[-len(top):])
                            stack = stack[:-len(top)] + [t]
                        else:
                            op_stack.append(top)
                tokens = tokens[1:]
        else:
            raise TokenError("unknown root token build mode")
        if len(stack) == 1:
            return stack[0]
        elif stack:
            raise TokenError("final stack contains more than 1 value")
        else:
            raise TokenError("final stack is empty")

    def parse(self, exp):
        '''
        Parse a string into a list of tokens that.
        Any parentheses is created as a nested expression which is stored internally in the object.

        Args:
            exp (str): a string notating the expression.

        Returns:
            (list): a list of tokens parsed from the expression string.
        '''
        if not isinstance(exp, str):
            raise TypeError("expected str, got " + exp.__class__.__name__)
        string = ""
        temp = ""
        looking = 0
        # nest parentheses as a seperate validation
        while exp:
            if looking:
                if exp[0] == "(": # additional open parentheses
                    looking += 1
                elif exp[0] == ")": # close parentheses found, ends when the number of open and close matches
                    looking -= 1
                if looking == 0: # ends when the number of open and close matches
                    (e, v) = self.new_expression(temp) # validate the sub expression
                    string += Expression.EXP_PREFIX + e
                    temp = ""
                else:
                    temp += exp[0]
            else:
                # open parentheses found, initating fetch
                if exp[0] == "(":
                    looking += 1
                else:
                    string += exp[0]
            exp = exp[1:]
        string = string.split()
        tokens = []
        # parse tokens
        for t in string:
            try: # float tokens are appended to the tokens list
                v = float(t)
                tokens.append(v)
            except ValueError: # non float token is added as string (could be either operator, variable or expression)
                if (t in self.operators) or (t[0] == Expression.VAR_PREFIX) or (t[0] == Expression.EXP_PREFIX):
                    tokens.append(t)
                else: # any variable that has no prefix is appended with the prefix
                    tokens.append(Expression.VAR_PREFIX + t)

        return tokens

    def new_expression(self, exp, operators=None, mode=None):
        '''
        Create a new expression from within the object.

        Args:
            exp (str, Expression): string notating the expression.
            operators (dict): operators set to be used by the new expression. (defaults to the set of current object)
            mode (int): expression mode to be used by the new expression. (defaults to the mode of current object)

        Returns:
            str: key of the new nested expression.
            Expression: the object of the new expression.
        '''
        key = self.__get_next_expression_key()
        if isinstance(exp, Expression):
            self.expression[key] = exp
        else:
            if not operators:
                operators = self.operators
            if not mode:
                mode = self.mode
            self.expression[key] = Expression(exp, expression=self.expression, operators=operators, mode=mode)
        return key, self.expression[key]

    def get_expression(self, key):
        '''
        Return a stored nested expression.

        Args:
            key (str): the key to obtain the nested expression.

        Returns:
            (Expression): stored expression stored as the key.
        '''
        if key in self.expression:
            v = self.expression[key]
            if isinstance(v, Expression):
                return v
        else:
            raise TokenError("unidentified expression '" + key + "'")

    def get_variable(self, key, **scope):
        '''
        Return the value of variable in the scope.

        Args:
            key (str): the key to obtain the nested expression.

        Kwargs:
            key, pair of variables' name and value.
        '''
        if key in scope:
            v = scope[key]
            if isinstance(v, Expression):
                return v.value(**scope)
            else:
                return v
        else:
            raise TokenError("unidentified token '" + key + "'")

    def value(self, **scope):
        '''
        Return the value of the expression.

        Positional arguments:
            tokens (list)  tokens to process as a value. (default the tokens in self)

        Kwargs:
            key, pair of variables' name and value.
        '''
        def get_val(token):
            retval = None
            if token.type == Expression.Token.TokenType.VALUE:
                val = token.value
                if isinstance(val, str): # string token
                    if val[0] == Expression.EXP_PREFIX: # expression token
                        retval = self.get_expression(val[1:]).value(**scope)
                    elif val[0] == Expression.VAR_PREFIX: # variable token
                        v = self.get_variable(val[1:], **scope)
                        if isinstance(v, Expression):
                            retval = v.value(**scope)
                        else:
                            retval = v
                    else: # if token is invalid
                        raise TokenError("unidentified token '" + val + "'")
                elif isinstance(val, float): # float token
                    retval = val
            elif token.type == Expression.Token.TokenType.OPERATOR:
                params = []
                for v in token.values:
                    params.append(get_val(v))
                retval = token.operator(*params)
            return retval
        return get_val(self.root)

    def __repr__(self):
        return self.text(self.mode)

    def text(self, mode=-1, **scope):
        '''
        Text representation of the expression
        Variable is passed as keyworded arguments.

        Args:
            mode (int): the arrangement of tokens to be displayed. (default infix)
            raw (bool): any nested expression will be shown as the key instead. (default false)

        Kwargs:
            key, pair of variables' name and value. non-existing variable will be printed as the string of the key.
        '''
        def get_text(val):
            retval = None
            if isinstance(val, str): # string token
                if val[0] == Expression.EXP_PREFIX: # expression token
                    retval = self.get_expression(val[1:]).text(mode, **scope)
                elif val[0] == Expression.VAR_PREFIX: # variable token
                    if val[1:] in scope:
                        v = self.get_variable(val[1:], **scope)
                        if isinstance(v, Expression):
                            retval = v.text(mode, **scope)
                        else:
                            retval = str(v)
                    else:
                        retval = val[1:]
                else: # if token is invalid
                    retval = val
            elif isinstance(val, float): # float token
                retval = str(val)
            elif isinstance(val, list): # list token
                retval = ""
                for t in val:
                    if retval:
                        retval += " "
                    retval += get_text(t) 
                retval = "(" + retval + ")"
            return retval
        if mode == -1:
            mode = self.mode
        tokens = self.root.collapse(mode)
        res = ""
        for t in tokens:
            if res:
                res += " "
            res += get_text(t)
        return res

    @staticmethod
    def Valid(exp, operators, mode):
        '''
        Validate an expression.

        Args:
            exp (str): a string notating the expression.
            operators (dict): operators set.
            mode (int): expression arrangement mode.

        Return:
            bool: true if the expression is valid.
            str: error message if not valid.
        '''
        mes = "valid"
        # recursive for parentheses
        valid = True
        string = ""
        temp = ""
        looking = 0
        # nest parentheses as a seperate validation
        while exp:
            if looking:
                if exp[0] == "(": # additional open parentheses
                    looking += 1
                elif exp[0] == ")": # close parentheses found
                    looking -= 1
                if looking == 0: # ends when the number of open and close matches
                    valid = valid and Expression.Valid(temp, operators, mode) # validate the sub expression
                    string += "1"
                    temp = ""
                else:
                    temp += exp[0]
            else:
                # open parentheses found, initating fetch
                if exp[0] == "(":
                    looking += 1
                else:
                    string += exp[0]
            exp = exp[1:]
        if looking:
            raise TokenError("expected token ')'")
        if valid:
            if mode == Expression.POSTFIX: # validate postfix
                val = 0
                for s in string.split():
                    if s in operators:
                        c = len(operators[s])
                        val -= c - 1
                        if val < 1:
                            mes = "unexpected operator '{}'".format(s)
                            valid = False
                            break
                    else:
                        val += 1
                if valid and val != 1:
                    mes = "{} tokens in the final stack".format(val)
                    valid = False
            if mode == Expression.INFIX: # validate infix
                val = 0
                poll = 0
                op = ""
                for s in string.split():
                    if s in operators:
                        c = len(operators[s])
                        if c > 1 and val < 1:
                            mes = "unexpected operator '{}'".format(s)
                            valid = False
                            break
                        if c > 1:
                            poll += c - 1
                        elif not op and val > 0:
                            mes = "unexpected operator '{}'".format(s)
                            valid = False
                            break
                        op = s
                    else:
                        val += 1
                        if poll > 0:
                            val -= poll
                            poll = 0
                        op = ""
                        if val != 1:
                            mes = "unexpected token '{}'".format(s)
                            valid = False
                            break
                if valid and op:
                    mes = "unsatisfied operator '{}'".format(op)
                    valid = False
                if valid and poll > 0:
                    mes = "expected {} more token(s)".format(poll)
                    valid = False
                if valid and val != 1:
                    mes = "{} tokens in the final stack".format(val)
                    valid = False
            if mode == Expression.PREFIX: # validate prefix
                poll = -1
                for s in string.split():
                    if s in operators:
                        c = len(operators[s])
                        if poll == -1:
                            poll = c
                        else:
                            if poll == 0:
                                mes = "unexpected operator '{}'".format(s)
                                valid = False
                                break
                            else:
                                poll += c - 1
                    else:
                        if poll < 1:
                            mes = "unexpected token '{}'".format(s)
                            valid = False
                            break
                        else:
                            poll -= 1
                if valid and poll > 0:
                    mes = "expected {} more token(s)".format(poll)
                    valid = False
        return valid, mes

def execute(*argv):
    '''
    Running as a standalone program.
    '''
    options = {"exp_mode":Expression.INFIX,
               "op_set":Expression.SET_DEFAULT,
               "show_syntax":False,
               "show_variable":False,
               "show_reduced":False,
               "show_expression":False
              }

    name = argv[0]

    def processOption(argv, opt):
        _args = []
        i = 1
        while i < len(argv):
            arg = argv[i]
            if arg:
                if arg[0] == "-":
                    if arg == "-post":
                        opt["exp_mode"] = Expression.POSTFIX
                    elif arg == "-pre":
                        opt["exp_mode"] = Expression.PREFIX
                    elif arg == "-in":
                        opt["exp_mode"] = Expression.INFIX
                    elif arg == "-sa":
                        opt["show_syntax"] = True
                        opt["show_variable"] = True
                        opt["show_reduced"] = True
                    elif arg == "-ss":
                        opt["show_syntax"] = True
                    elif arg == "-sv":
                        opt["show_variable"] = True
                    elif arg == "-sr":
                        opt["show_reduced"] = True
                    else:
                        print("invalid option:", arg)
                        exit()
                else:
                    _args.append(arg)
            i += 1
        return _args

    args = processOption(argv, options)

    printv = partial(print, name+":")

    if args:
        valid, mes = Expression.Valid(args[0], operators=options["op_set"], mode=options["exp_mode"])
        if not valid:
            printv(mes)
            exit()

        exp = Expression(Expression.fixformat(args[0], operators=options["op_set"]), operators=options["op_set"], mode=options["exp_mode"])
        args = args[1:]
        val = {}
        for s in args:
            sp = s.split("=")
            try:
                val[sp[0]] = float(sp[1])
            except ValueError:
                valid, mes = Expression.Valid(sp[1], operators=options["op_set"], mode=options["exp_mode"])
                if not valid:
                    printv(mes)
                    exit()
                val[sp[0]] = Expression(Expression.fixformat(sp[1], operators=options["op_set"]), operators=options["op_set"], mode=options["exp_mode"])

        try:
            print_mode = Expression.INFIX
            if options["show_syntax"]:
                print("syntax:", exp.text(print_mode))
            if val and options["show_variable"]:
                print("where:")
            if val and options["show_variable"]:
                for k, v in val.items():
                    print(k, "=", v)
            if options["show_reduced"]:
                print("reduced:", exp.text(print_mode, **val))
            print("result:", exp.value(**val))
        except RecursionError:
            printv("endless recursive syntax")
        except TokenError as e:
            printv(e.__class__.__name__ + ":", e.msg)

if __name__ == "__main__":
    execute(*sys.argv)
