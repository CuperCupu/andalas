import math

def __add(x, y):
    if isinstance(x, list):
        n = []
        for i in range(0, len(x)):
            n.append(x[i] + y)
        return n
    else:
        return x + y

def __sub(x, y):
    if isinstance(x, list):
        n = []
        for i in range(0, len(x)):
            n.append(x[i] - y)
        return n
    else:
        return x - y

def __mul(x, y):
    if isinstance(x, list):
        n = []
        for i in range(0, len(x)):
            n.append(x[i] * y)
        return n
    else:
        return x * y

def __div(x, y):
    if isinstance(x, list):
        n = []
        for i in range(0, len(x)):
            n.append(x[i] / y)
        return n
    else:
        return x / y

def __pow(x, y):
    if isinstance(x, list):
        n = []
        for i in range(0, len(x)):
            n.append(x[i] ** y)
        return n
    else:
        return x ** y

def __mod(x, y):
    if isinstance(x, list):
        n = []
        for i in range(0, len(x)):
            n.append(x[i] % y)
        return n
    else:
        return x % y

SET_STANDARD = {
    "+":__add,
    "-":__sub,
    "*":__mul,
    "/":__div,
    "^":__pow,
    "%":__mod,
    "add":__add,
    "sub":__sub,
    "mul":__mul,
    "div":__div,
    "pow":__pow,
    "mod":__mod,
    "sqr":(lambda x: __pow(x, 2)),
    "sqrt":(lambda x: __pow(x, (0.5))),
    "abs":abs,
    "sign":(lambda x: 1 if x > 0 else (-1 if x < 0 else 0)),
}

SET_BOOLEAN = {
    "one":(lambda x: 1 if x!= 0 else 0),
    "not":(lambda x: 0 if x != 0 else 1),
    "and":(lambda x, y: 1 if x and y else 0),
    "nand":(lambda x, y: 0 if x and y else 1),
    "or":(lambda x, y: 1 if x or y else 0),
    "nor":(lambda x, y: 0 if x or y else 1),
    "xor":(lambda x, y: 1 if ((x or y) and (not x or not y)) else 0),
    "eq":(lambda x, y: 1 if ((x and y) or (not x and not y)) else 0),
    "neq":(lambda x, y: 0 if ((x and y) or (not x and not y)) else 1),
    "<":(lambda x, y: x < y),
    "<=":(lambda x, y: x <= y),
    ">":(lambda x, y: x > y),
    ">=":(lambda x, y: x >= y),
    "==":(lambda x, y: x == y),
    "!=":(lambda x, y: x != y),
}

def polinom(l, x):
    if isinstance(l, list):
        i = 0
        n = []
        for c in l:
            n.append(c * (x ** i))
            i += 1
        return n
    else:
        raise TypeError("expected type 'list' got {}".format(x.__class__.__name__))

SET_MATH = {
    "sin":(lambda x: math.sin(x)),
    "cos":(lambda x: math.cos(x)),
    "tan":(lambda x: math.tan(x)),
    "sinh":(lambda x: math.sinh(x)),
    "cosh":(lambda x: math.cosh(x)),
    "tanh":(lambda x: math.tanh(x)),
    "asin":(lambda x: math.asin(x)),
    "acos":(lambda x: math.acos(x)),
    "atan":(lambda x: math.atan(x)),
    "asinh":(lambda x: math.asinh(x)),
    "acosh":(lambda x: math.acosh(x)),
    "atanh":(lambda x: math.atanh(x)),
    "ceil":(lambda x: math.ceil(x)),
    "floor":(lambda x: math.floor(x)),
    "log":(lambda x, b: math.log(x, b)),
    "rad":(lambda x: math.radians(x)),
    "deg":(lambda x: math.degrees(x)),
    "fac":(lambda x: math.factorial(x)),
}

SET_EXTENDED = {}
SET_EXTENDED.update(SET_STANDARD)
SET_EXTENDED.update(SET_BOOLEAN)
SET_EXTENDED.update(SET_MATH)

SET_LIST = {
    "at":lambda x, y: x[int(y)],
    "len":len,
    "polinom":polinom,
    "sum":(lambda x: sum(x, 0)),
    "max":(lambda x: max(x)),
    "min":(lambda x: min(x)),
    "rev":(lambda x: list(reversed(x))),
}

CONST = {
    "PI":math.pi,
    "E":math.e,
}