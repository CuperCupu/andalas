#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Andalas script interpreter.
'''
import os
import sys
from pathlib import Path
from functools import reduce
from functools import partial
# Add current path to sys path search list
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from expression import Expression
import package
import operators

class Script:
    '''
    A script object for andalas.
    A standalone class to support script loading.
    '''

    def __init__(self, script):
        self.script = script
        self.scope = {}
        self.args = []

    def __call__(self, *args):
        ipt = Interpreter()
        ipt.scope.update(self.scope)
        if self.args:
            diff = len(self.args) - len(args)
            if diff > 0:
                raise TypeError("missing {} argument{}".format(diff, "s" if diff > 1 else ""))
            elif diff < 0:
                raise TypeError("takes {} argument{} but {} {} given".format(len(self.args), "s" if len(self.args) > 1 else "", len(args), "was" if len(args) == 1 else "were"))
        
        retval = None
        lcount = 0
        try:
            for line in self.script:
                lcount += 1
                line = line.strip()
                codes = line.split(Interpreter.SEPERATOR)
                for code in codes:
                    if code:
                        if code[:6] == "return":
                            return ipt.interpret(code.strip())
                        else:
                            ipt.interpret(code.strip())
        except Exception as e:
            if hasattr(e, "msg"):
                print(e.__class__.__name__+":", getattr(e, "msg"), "at line:")
            else:
                print(e.__class__.__name__, "at line:")
            print("  ", str(lcount) + ".", code)
            return retval

class Interpreter:
    '''
    An interpreter object.
    Each interpreter has it's own scope.
    '''

    NOTATION_STRING = ['"', '"']
    NOTATION_EXPRESSION = ['(', ')']
    NOTATION_LIST = ['[', ']']

    @staticmethod
    def wrap_exp_literal(string):
        return Interpreter.NOTATION_EXPRESSION[0] + str(string) + Interpreter.NOTATION_EXPRESSION[1]

    @staticmethod
    def is_exp_literal(string):
        return string[0] == Interpreter.NOTATION_EXPRESSION[0] and string[len(string)- 1] == Interpreter.NOTATION_EXPRESSION[1]

    @staticmethod
    def wrap_str_literal(string):
        return Interpreter.NOTATION_STRING[0] + str(string) + Interpreter.NOTATION_STRING[1]

    @staticmethod
    def is_str_literal(string):
        return string[0] == Interpreter.NOTATION_STRING[0] and string[len(string)- 1] == Interpreter.NOTATION_STRING[1]

    @staticmethod
    def wrap_list_literal(string):
        return Interpreter.NOTATION_LIST[0] + str(string) + Interpreter.NOTATION_LIST[1]

    @staticmethod
    def is_list_literal(string):
        return string[0] == Interpreter.NOTATION_LIST[0] and string[len(string)- 1] == Interpreter.NOTATION_LIST[1]


    def set_mode(self, *args):
        if len(args) == 1:
            if Interpreter.is_str_literal(args[0]):
                mode_name = args[0][1:][:-1]
            else:
                mode_name = args[0]
            if mode_name == "postfix":
                self.mode = Expression.POSTFIX
            elif mode_name == "prefix":
                self.mode = Expression.PREFIX
            elif mode_name == "infix":
                self.mode = Expression.INFIX
            else:
                raise SyntaxError("unidentified mode name '{}'".format(mode_name))
        elif len(args) < 1:
            raise SyntaxError("expected mode name after")
        elif len(args) > 1:
            raise SyntaxError("unidentified token '{}'".format(args[1]))

    def has_val(self, key):
        try:
            v = float(key)
            return True
        except ValueError:
            return Interpreter.is_exp_literal(key) or\
                   Interpreter.is_str_literal(key) or\
                   Interpreter.is_list_literal(key) or\
                   key in self.scope or key in self.const

    def get_val(self, key):
        try:
            v = float(key)
            return v
        except ValueError:
            pass
        if Interpreter.is_exp_literal(key):
            exp = key[1:][:-1]
            e = Expression(exp, operators=self.set, mode=self.mode)
            return e
        elif Interpreter.is_str_literal(key):
            s = key[1:][:-1]
            return s
        elif Interpreter.is_list_literal(key):
            l = key[1:][:-1]
            tokens = self.parse_args(l)
            l = []
            for t in tokens:
                if self.has_val(t):
                    l.append(self.get_val(t))
                else:
                    raise SyntaxError("unidentified token '{}'".format(t))
            return l
        elif key in self.scope:
            v = self.scope[key]
            return v
        elif key in self.const:
            v = self.const[key]
            return v

    def printv(self, sep="", ender="\n", raw=False, *args):
        sval = ""
        for arg in args:
            if sval:
                sval += sep
            try:
                v = float(arg)
                sval += str(v)
                continue
            except ValueError:
                pass
            if self.has_val(arg):
                v = self.get_val(arg)
                if isinstance(v, Expression):
                    if raw:
                        sval += Interpreter.wrap_exp_literal(v.text())
                    else:
                        sval += str(self.get_exp_val(v))
                else:
                    if raw and isinstance(v, str) and v.strip():
                        sval += Interpreter.wrap_str_literal(str(v))
                    else:
                        sval += str(v)
            else:
                raise SyntaxError("variable '{}' not declared".format(arg))
        print(sval, end=ender)

    def let(self, ftoken, raw, *args):
        if not args:
            raise SyntaxError("expected variable name")
        varl = 0
        while varl < len(args) and args[varl] not in ftoken:
            varl += 1
        if varl >= len(args):
            keywords = ""
            for kw in ftoken[:-1]:
                if keywords:
                    keywords += ", "
                keywords += "'" + kw + "'"
            if keywords:
                keywords += " or "
            keywords += "'" + ftoken[len(ftoken) - 1] + "'"
            raise SyntaxError("expected keyword'{}' '{}'".format("s" if len(ftoken) > 1 else "", keywords))
        elif varl == len(args) - 2:
            exp = args[len(args) - 1]
            if self.has_val(exp):
                v = self.get_val(exp)
                ret = v
                for i in range(0, varl):
                    self.scope[args[i]] = v
                    if not raw:
                        ret = self.settle(args[i])
                return ret
            else:
                raise SyntaxError("unidentified token '{}'".format(exp))
        else:
            if varl < len(args) - 2:
                raise SyntaxError("unexpected token '{}'".format(args[varl + 2]))
            else:
                raise SyntaxError("expected expression")

    def get_exp_val(self, exp):
        try:
            return exp.value(**self.scope)
        except SyntaxError as e:
            raise SyntaxError("{}".format(e.msg))
        except Exception as e:
            raise SyntaxError("{}".format(str(e)))

    def get(self, *args):
        retval = []
        for arg in args:
            try:
                v = float(arg)
                retval.append(v)
                continue
            except ValueError:
                pass
            if self.has_val(arg):
                v = self.get_val(arg)
                if isinstance(v, Expression):
                    retval.append(self.get_exp_val(v))
                else:
                    retval.append(v)
            else:
                raise SyntaxError("variable '{}' not declared".format(arg))
        if len(retval) == 1:
            return retval[0]
        elif len(retval) > 1:
            return tuple(retval)

    def settle(self, key):
        v = self.get_val(key)
        if v:
            if isinstance(v, Expression):
                v = self.get_exp_val(v)
                if key in self.scope:
                    self.scope[key] = v
                return v
            else:
                return v
        else:
            return key


    def __init__(self):
        self.scope = {}
        self.const = {
            "\\n":"\n"
        }
        self.func = {}
        self.mode = Expression.INFIX
        self.set = operators.SET_EXTENDED
        self.set.update(operators.SET_LIST)
        self.func["print"] = partial(self.printv, " ", "\n", False)
        self.func["show"] = partial(self.printv, " ", "\n", True)
        self.func["printr"] = partial(self.printv, "", "", False)
        self.func["let"] = partial(self.let, ["be","="], True)
        self.func["set"] = partial(self.let, ["to"], False)
        self.func["mode"] = self.set_mode
        self.func["return"] = self.get
        self.func["settle"] = self.settle

    @staticmethod
    def parse_args(args):
        argv = []
        token = ""
        text = False
        exp = 0
        listp = 0
        for s in args:
            if listp:
                token += s
                if s == Interpreter.NOTATION_LIST[0]:
                    listp += 1
                elif s == Interpreter.NOTATION_LIST[1]:
                    listp -= 1
            elif not token and s == Interpreter.NOTATION_LIST[0]:
                listp += 1
                token += s
            elif exp:
                token += s
                if s == Interpreter.NOTATION_EXPRESSION[0]:
                    exp += 1
                elif s == Interpreter.NOTATION_EXPRESSION[1]:
                    exp -= 1
            elif not token and s == Interpreter.NOTATION_EXPRESSION[0]:
                exp += 1
                token += s
            elif text:
                token += s
                if s == Interpreter.NOTATION_STRING[1]:
                    text = False
            elif not exp and s == Interpreter.NOTATION_STRING[0]:
                if token:
                    raise SyntaxError("unexpected token '{}'".format(Interpreter.NOTATION_STRING[0]))
                else:
                    text = True
                    token = s
            elif s == " ":
                if token:
                    argv.append(token)
                    token = ""
            else:
                token += s
        if listp:
            raise SyntaxError("expected token '{}'".format(Interpreter.NOTATION_LIST[1]))
        if exp:
            raise SyntaxError("expected token '{}'".format(Interpreter.NOTATION_EXPRESSION[1]))
        if text:
            raise SyntaxError("expected token '{}'".format(Interpreter.NOTATION_STRING[1]))
        if token:
            argv.append(token)
            token = ""
        return argv

    def fixformat(self, string):
        i = string.find(Interpreter.NOTATION_LIST[0])
        pos = 0
        while i > 0:
            if string[i - 1] != Interpreter.NOTATION_LIST[0] and string[i - 1] != " ":
                string = string[:i] + " " + string[i:]
                pos += 1
            pos = i + 1
            i = string.find(Interpreter.NOTATION_LIST[0], pos)
        i = string.find(Interpreter.NOTATION_LIST[1])
        pos = 0
        while i > 0 and i < len(string) - 1:
            if string[i + 1] != Interpreter.NOTATION_LIST[1] and string[i + 1] != " ":
                string = string[:i+1] + " " + string[i+1:]
                pos += 1
            pos += i + 1
            i = string.find(Interpreter.NOTATION_LIST[1], pos)
        i = string.find("=")
        pos = 0
        while i > 0 and i < len(string):
            if string[i - 1] != " ":
                poss = i - 1
                while poss > 0 and poss != " ":
                    poss -= 1
                tok = string[poss:i+1]
                if tok not in self.set:
                    string = string[:i] + " " + string[i:]
                    pos += 1
                    i += 1
            if i < len(string) - 1 and string[i + 1] != " ":
                poss = i + 1
                while poss < len(string) and poss != " ":
                    poss += 1
                tok = string[i:poss+1]
                if tok not in self.set:
                    string = string[:i+1] + " " + string[i+1:]
            pos = i + 1
            i = string.find("=", pos)
        return string

    def interpret(self, args):
        if not args:
            return None
        i = args.find(" ")
        if i == -1 and args in self.func:
            return self.func[args]()
        else:
            argv = self.fixformat(args)
            argv = Interpreter.parse_args(argv)
            if argv[0] in self.func:
                fname = argv[0]
                args = args[len(fname) + 1:]
                return self.func[fname](*argv[1:])
            elif len(argv) > 1 and argv[1] == "=":
                if len(argv) > 2:
                    if len(argv) > 3:
                        exp = Interpreter.wrap_exp_literal(reduce(lambda x, y: str(x) + " " + str(y), argv[2:]))
                    else:
                        exp = argv[2]
                    if self.has_val(exp):
                        v = self.get_val(exp)
                        self.scope[args[0]] = v
                        return self.scope[args[0]]
                    else:
                        raise SyntaxError("unidentified token {}".format(exp))
                else:
                    raise SyntaxError("expected expression")
            elif len(argv) == 1 and self.has_val(argv[0]):
                v = self.get_val(argv[0])
                if isinstance(v, Expression):
                    v = self.get_exp_val(v)
                return v
            else:
                arg = reduce(lambda x, y: str(x) + " " + str(y), argv)
                exp = Expression.fixformat(self.fixformat(arg), self.set)
                valid, mes = Expression.Valid(exp, self.set, self.mode)
                if valid:
                    e = Expression(exp, mode=self.mode, operators=self.set)
                    return self.get_exp_val(e)
                else:
                    raise SyntaxError("{} while parsing '{}'".format(mes, arg))

    SEPERATOR = ";"

    @staticmethod
    def run_script(filename):
        p = Path(filename)
        if p.is_file():
            f = open(filename)
            lines = []
            for line in f:
                lines.append(line)
            f.close()
            script = Script(lines)
            return script()
        else:
            raise FileNotFoundError("No such file: {}".format(filename))

def execute(*argv):
    if len(argv) == 1:
        running = True
        cli = Interpreter()
        while running:
            sys.stdout.write("~~~ ")
            sys.stdout.flush()
            s = input()
            s = s.lower()
            if s == "exit":
                running = False
            else:
                try:
                    v = cli.interpret(s)
                    if v != None:
                        print(v)
                except SyntaxError as e:
                    if hasattr(e, "msg"):
                        print(e.__class__.__name__+":", e.msg)
    elif len(argv) > 1:
        filename = argv[1]
        try:
            v = Interpreter.run_script(filename)
            if v != None:
                print("returned:", v)
        except Exception as e:
            print(e.__class__.__name__ + ":", str(e))

def main(args=None):
    if not args:
        args = sys.argv
        print(package.name, package.version, "by.", package.author)
        print("You can contact me via:")
        print(package.author_email)
        print(package.url)
        print("Your feedback is appreciated.")
    execute(*args)

if __name__ == "__main__":
    main()
